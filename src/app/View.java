package app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.freixas.jcalendar.DateListener;
import org.freixas.jcalendar.JCalendar;

import com.l2fprod.common.swing.JButtonBar;
import com.l2fprod.common.swing.JTipOfTheDay;
import com.l2fprod.common.swing.tips.DefaultTip;
import com.l2fprod.common.swing.tips.DefaultTipModel;


	   class View extends JFrame {
	private static final long serialVersionUID = 2159339392349431070L;
	private JMenuBar menuBar = new JMenuBar();
	private JLabel lKolumna = new JLabel("Kolumna:");
	private JLabel lLiczba = new JLabel("Liczba:");
	private JLabel lWiersz = new JLabel("Wiersz:");
	private JButton btnSave = new JButton("Zapisz");
	private JButton btnClean = new JButton("Wyzeruj");
	private JButton btnMove = new JButton("Przenieś");
	private JButton btnDraw = new JButton("Wykres");
	private JScrollPane scrollPane_table = new JScrollPane();
	private JScrollPane scrollPane_JList = new JScrollPane();
	private JScrollPane scrollPane_textArea = new JScrollPane();
	private JTextField textField = new JTextField();;
	private JTable table = new JTable();
	private JSpinner sKolumna = new JSpinner();
	private JSpinner sWiersz = new JSpinner();
	private JTextArea textArea = new JTextArea();
	private JList<Object> list = new JList<Object> (Constants.Options);
	private JPanel North = new JPanel();
    private JPanel West = new JPanel();
    private JPanel East = new JPanel();
	private JPanel South = new JPanel();
	private JPanel Center = new JPanel();
	private JToolBar bar = new JToolBar();
	private JMenu info = new JMenu("");
	private JMenu instr = new JMenu("");
	private JMenuItem author = new JMenuItem("Autor");
	private JMenuItem help = new JMenuItem("Pomoc");
	private JMenu file = new JMenu("");
	private SaveFile zapisz = new SaveFile();
	private JButtonBar buttonBar = new JButtonBar();
	private JCalendar calendar = new JCalendar();
	private DefaultTipModel spis_porad = new DefaultTipModel();
	
	
	    View() {
	    	
		setTitle("Programowanie aplikacji");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		  int screenHeight = dim.height;
		  int screenWidth =  dim.width;
		// Ustawienie szerokości i wysokości ramki oraz polecenie systemowi, aby ustalił jej położenie
		setSize(screenWidth / 2, screenHeight / 2);
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
	
        file.setIcon(new ImageIcon(View.class.getResource("file.png")));
        instr.setIcon(new ImageIcon(View.class.getResource("help.png")));
        info.setIcon(new ImageIcon(View.class.getResource("info.png")));
        
        setJMenuBar(menuBar);
		menuBar.add(file);
		menuBar.add(instr);
		menuBar.add(info);
		instr.add(help);
		info.add(author);
		
		getContentPane().add(North, BorderLayout.NORTH);
		getContentPane().add(West, BorderLayout.WEST);
		getContentPane().add(East, BorderLayout.EAST);
		getContentPane().add(Center, BorderLayout.CENTER);
		getContentPane().add(South, BorderLayout.SOUTH);
		
		East.add(bar);
		bar.add(btnSave);
		buttonBar.add(btnClean);
		buttonBar.add(btnMove);
		buttonBar.add(btnDraw);
		
		scrollPane_textArea.setBounds(10, 320, 376, 100);
		scrollPane_table.setBounds(10, 200, 376, 100);
		scrollPane_JList.setBounds(396, 340, 80, 59);
		sKolumna.setBounds(396, 225, 44, 20);
		sWiersz.setBounds(396, 280, 44, 20);
		textField.setBounds(10, 45, 86, 20);
		lLiczba.setBounds(10, 20, 46, 14);
		lKolumna.setBounds(396, 200, 65, 14);
		lWiersz.setBounds(396, 256, 65, 14);
		buttonBar.setBounds(221, 170, 165, 20);
		calendar.setBounds(486, 229, 325, 170);
		
		scrollPane_table.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPane_table.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_table.setViewportView(table);
		scrollPane_JList.setViewportView(list);
		scrollPane_textArea.setViewportView(textArea);
		
		table.setModel(Model.tablemodel);
		table.setCellSelectionEnabled(false);
		table.setFocusable(false);

		sKolumna.setModel(Model.spinnermodelcolumn);
		sWiersz.setModel(Model.spinnermodelrow);
		
		list.setVisibleRowCount(3);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		Center.setLayout(null);
		Center.add(textField);
		Center.add(lLiczba);
		Center.add(lKolumna);
		Center.add(scrollPane_table);
		Center.add(sKolumna);
		Center.add(sWiersz);
		Center.add(lWiersz);
		Center.add(scrollPane_JList);
		Center.add(scrollPane_textArea);
		Center.add(buttonBar);
		Center.add(calendar);
		
		spis_porad.add(new DefaultTip("tip1","Treść naszej pierwszej porady."));
    	spis_porad.add(new DefaultTip("tip2","Treść naszej drugiej porady."));
    	JTipOfTheDay tip = new JTipOfTheDay(spis_porad);
    	tip.showDialog(this);
    	
		
	    }
	    
	    public double getTextFieldNumber() {
	        return Double.parseDouble(textField.getText());
	    }
	    
	    public int getRowNumber() {
	    	return (int) sWiersz.getValue();
	    }
	    
	    public int getColumnNumber() {
	    	return (int) sKolumna.getValue();
	    }
	    
	    public DefaultTableModel TableModel() {
	    	Model.tablemodel = (DefaultTableModel) table.getModel();
			return Model.tablemodel;
	    } 
	    
	    public int getIndexList() {
	    	return list.getSelectedIndex();
	    }
	    
	    public String dateGet() {
	    	return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getDate());
	    }  

	    void textAppendSave() {
				try {
					zapisz.Save(textArea);
				} catch (Exception e) {
					e.printStackTrace();
				}}
	    
	    void cleanTextArea() {
	    	textArea.selectAll();
            textArea.replaceSelection("");
	    }
	    
	    void textAppendSuma(double suma) {
	    	textArea.append("Suma elementów wynosi: " + suma + "\n");
	    }
	    
	    void textAppendAverage(double average) {
	    	textArea.append("Średnia elementów wynosi: " + average + "\n");
	    }
	    
	    void textAppendMinMax(double[] wynik) {
	    	textArea.append("Maximum elementów: " + wynik[1] + "\n");
			textArea.append("Minimum elementów: " + wynik[0] + "\n");
	    }
	    
	    void textAppendCalendar() {
	    	textArea.append("Data zmieniona na: " + dateGet() + "\n");
	    }
	    
	    void addDialogAutorListener(ActionListener listener) {
	    	author.addActionListener(listener);
	    }
	    
	    void addDialogHelpListener(ActionListener listener) {
	    	help.addActionListener(listener);
	    }
	    
	    void addActionListener(ActionListener listener) {
	        btnMove.addActionListener(listener);
	    }
	    
	    void addSaveActionListener(ActionListener listener) {
	    	btnSave.addActionListener(listener);
	    }
	    
	    void addButtonCleanListener(ActionListener listener) {
	    	btnClean.addActionListener(listener);
	    }
	    
	    void addButtonDrawListener(ActionListener listener) {
	    	btnDraw.addActionListener(listener);
	    }
	    
	    void addChangeListenerColumn(ChangeListener listener) {
	    	sKolumna.addChangeListener(listener);
	    }
	    
	    void addChangeListenerRow(ChangeListener listener) {
	    	sWiersz.addChangeListener(listener);
	    }
	    
	    void addListListener(ListSelectionListener listener) {
	    	list.addListSelectionListener(listener);
	    }
	    
	    void addCalendarListener(DateListener listener) {
	    	calendar.addDateListener(listener);
	    }
}
	   