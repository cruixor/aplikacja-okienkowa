package app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.InvalidAlgorithmParameterException;

import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;

public class Controller{
	
	private Model model;
	private View view;
	
	public Controller(Model model , View view) {
		  
		  this.view  = view;
	      this.model = model;
	      
		  view.addActionListener(new TableListener());
		  view.addChangeListenerColumn(new SpinnerColumn());
		  view.addChangeListenerRow(new SpinnerRow());
		  view.addListListener(new JListListener());
		  view.addButtonCleanListener(new CleanButton());
		  view.addSaveActionListener(new SaveButton());
		  view.addDialogAutorListener(new DialogAutorActionListener());
		  view.addDialogHelpListener(new DialogHelpActionListener());
		  view.addCalendarListener(new CalendarListener());
		  view.addButtonDrawListener(new DrawButton());
		}
	  
class TableListener implements ActionListener {	
	public void actionPerformed(ActionEvent e) {
	    	try {
	    		if (view.getTextFieldNumber() <= 0)
	    			 throw new InvalidAlgorithmParameterException();
	    	model.setValue(view.TableModel() , view.getTextFieldNumber());
			model.ArraySave(view.getTextFieldNumber());
	        }catch(NumberFormatException e1) {
	    	JOptionPane.showMessageDialog(null, "Inne znaki niż cyfry są niedozwolone.");  //Obsługa błędów.
	        }catch(InvalidAlgorithmParameterException e1) {
	        	JOptionPane.showMessageDialog(null, "Program nie obsługuje liczb ujemnych.");  //Obsługa błędów.
	    }
	}
		}

class SpinnerColumn implements ChangeListener {
	public void stateChanged(ChangeEvent e) {
		model.SpinnerActionColumn(view.getColumnNumber());
		}
	}

class SpinnerRow implements ChangeListener {
	public void stateChanged(ChangeEvent e) {
		model.SpinnerActionRow(view.getRowNumber());
		}
	}

class JListListener implements ListSelectionListener {
	public void valueChanged(ListSelectionEvent e) {
		int index = view.getIndexList();
		if (index == 0) {
			if (!e.getValueIsAdjusting()) {
				view.textAppendSuma(model.Suma());
			}}
		if (index == 1) {
			if (!e.getValueIsAdjusting()) {
				view.textAppendAverage(model.Average());
			}}
		if (index == 2) {
			if (!e.getValueIsAdjusting()) { 
				view.textAppendMinMax(model.MinMax());
			}}}
		
		}

class CleanButton implements ActionListener {
	public void actionPerformed(ActionEvent e) {
		view.cleanTextArea();
		model.tableCleaner(view.TableModel());
		model.arrayCleaner();
		}}


class SaveButton implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	view.textAppendSave();
	}}

class DrawButton implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	model.barChart();
	}}

class DialogAutorActionListener implements ActionListener {
    public void actionPerformed(ActionEvent e){
    	model.autorDialog();
	 	}} 	 
    
class DialogHelpActionListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
    	model.helpDialog();
    }}

class CalendarListener implements DateListener {
	public void dateChanged(DateEvent arg0) {
		view.textAppendCalendar();
	}}



	}

