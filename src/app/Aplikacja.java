package app;

public class Aplikacja {

			public static void main(String[] args) {
				 java.awt.EventQueue.invokeLater(new Runnable() {
				        public void run() {
				        	try {
				        		Model theModel = new Model();
				        		View theView = new View();
				        		Controller theController = new Controller(theModel, theView);
				                theView.setVisible(true); 
				            } catch (Exception e) {
				        	e.printStackTrace();
				            }
				        	}
				 		});
					}
				}