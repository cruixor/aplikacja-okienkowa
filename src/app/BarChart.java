package app;

import javax.swing.JDialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;

public class BarChart extends JDialog{
	private static final long serialVersionUID = -3760117606486368221L;

	public BarChart(CategoryDataset DataSet) {

    JFreeChart barChart = ChartFactory.createBarChart ("Histogram poziomy", "Kolumny", "Wartości", DataSet , PlotOrientation.VERTICAL, true, true, false);
    ChartPanel chartPanel = new ChartPanel( barChart );        
    chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );        
    setContentPane( chartPanel ); 
}}