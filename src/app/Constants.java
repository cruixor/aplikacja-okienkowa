package app;

public class Constants {

	public static final String[] TABLE_HEADER = {"C0", "C1", "C2", "C3", "C4"};

	public static final Object[][] DATA = {
		                  {0, 0, 0, 0, 0},
			              {0, 0, 0, 0, 0},
			              {0, 0, 0, 0, 0},
			              {0, 0, 0, 0, 0},
			              {0, 0, 0, 0, 0}
	                     				  };
	public static final String[] Options = {"SUMA", "ŚREDNIA", "MAX/MIN"};
	
	public static final int value = 0;
	
	public static final int minimum = 0;
	
	public static final int maximum = 4;
	
	public static final int stepSize = 1;
}

