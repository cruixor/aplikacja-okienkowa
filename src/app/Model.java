package app;

import java.util.Arrays;

import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

public class Model{

private double tablica[][] = new double[5][5];	
private int i,j = 0;
private DialogAutor autor = null;
private DialogHelp help = null;
private BarChart chart = null;
public static DefaultTableModel tablemodel = new DefaultTableModel(Constants.DATA, Constants.TABLE_HEADER);
public static SpinnerNumberModel spinnermodelcolumn = new SpinnerNumberModel(Constants.value , Constants.minimum , Constants.maximum , Constants.stepSize);
public static SpinnerNumberModel spinnermodelrow = new SpinnerNumberModel(Constants.value , Constants.minimum , Constants.maximum , Constants.stepSize);	

	void ArraySave(Object textfield) {
		tablica[i][j] = (double) textfield;
	}
    
    void SpinnerActionColumn(Object getColumnNumber) {
		i = (int) getColumnNumber;
	}
    
    void SpinnerActionRow(Object getRowNumber) {
    	j = (int) getRowNumber;
    }
    
    void setValue(DefaultTableModel tablemodel , Object textfield) {
    	tablemodel.setValueAt(textfield, j, i);
    }
    
    void tableCleaner(DefaultTableModel tablemodel) {
	    for (int row = 0; row < tablemodel.getColumnCount(); row++) {
	    for (int cols = 0; cols < tablemodel.getRowCount(); cols++) {
	    	tablemodel.setValueAt(0, row, cols);
	        }
	    }
    }
    
    void autorDialog () {
    	if(autor != null) autor.setVisible(true);
	 	else {
    	DialogAutor autor = new DialogAutor();
    	autor.setVisible(true);
    	}}
    
    void helpDialog() {
    	if(help != null) help.setVisible(true);
    	else {
    		DialogHelp help = new DialogHelp();
    		help.setVisible(true);
    	}}
    
    void barChart() {
    	if(chart != null) chart.setVisible(true);
    	else {
    		BarChart chart = new BarChart(createDataset());
    		chart.pack( );        
    	    RefineryUtilities.centerFrameOnScreen(chart); 
    		chart.setVisible(true);
    	}}
    
    void arrayCleaner() {
    	for (double[] row : tablica) 
    		Arrays.fill(row, 0);
    }
    
    public double Suma() {
    	double suma = 0;
	    for (int row = 0; row < tablica.length; row++) {
	    for (int cols = 0; cols < tablica[row].length; cols++) {
	        suma += tablica[row][cols];
	        }
	    }
	    
	    return suma;
    }
    
    public double Average() {
    	double total = 0;
		double average = 0;
	    for (int row = 0; row < tablica.length; row++) {
	    for (int cols = 0; cols < tablica[row].length; cols++) {
	        total += tablica[row][cols];
	        if (tablica[row][cols] != 0) {
	        	average++;
	        }
	    }
	 }
	    try {
        average = total/average;
	    }catch (ArithmeticException e1) {
	    	average = 0;
	    }
	    
	    return average;
    }
    
    public double[] MinMax() {
    	double minValue = tablica[0][0];
		double maxValue = tablica[4][4];
        for(int i = 0; i < tablica.length; i++) {
        	for (int j = 0; j <tablica[i].length; j++) {
        		if (tablica[i][j] < minValue) {
        			minValue = tablica[i][j];
        		}
        		if (tablica[i][j] > maxValue) {
        			maxValue = tablica[i][j];
        		}
        	}
        }
          double[] wynik = {minValue, maxValue};
          return wynik;
    }
    
    public CategoryDataset createDataset() { 
        String rzad;
        String kolumna;
    	DefaultCategoryDataset dataset = new DefaultCategoryDataset();  
        for (int row = 0; row < tablica.length; row++) {
        	rzad = Integer.toString(row);
    	    for (int cols = 0; cols < tablica[row].length; cols++) {
    	    	kolumna = Integer.toString(cols);
    	    	dataset.addValue(tablica[row][cols], kolumna , rzad ); 
    	        }}
    	    return dataset; 
    }
             
}

