package app;


import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;

public class DialogHelp extends JDialog {

	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Konstruktor bezparametrowy klasy <code>HelpWindow</code>
	 */
	public DialogHelp() {
		this.setTitle("Pomoc - aplikacja testowa");
		this.setModal(true);
		this.setResizable(true);
		this.setSize(800,600);
	
		this.addWindowListener	(new WindowAdapter(){ 
			public void windowClosing(WindowEvent e){  
				setVisible(false);				
			}	
		});	
		
		
		Dimension dialogSize = getSize();		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  
		if(dialogSize.height > screenSize.height) 
			dialogSize.height = screenSize.height;
		if(dialogSize.width > screenSize.width)
			dialogSize.height = screenSize.width;
			
		setLocation((screenSize.width-dialogSize.width)/2,   
						(screenSize.height-dialogSize.height)/2);
	}
	
	
}