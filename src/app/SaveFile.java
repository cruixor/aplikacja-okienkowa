package app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;


public class SaveFile extends JFrame {

	private static final long serialVersionUID = -2924828349990474242L;
	
	void Save (JTextArea textArea) {
		JFileChooser fileChooser = new JFileChooser();
       
		fileChooser.setMultiSelectionEnabled(false);  //W tym miejscu zabraniam apletowi wyboru kilku pozycji w oknie wyszukiwania.
		fileChooser.setAcceptAllFileFilterUsed(false);  //W tym miejscu zabraniam apletowi wyboru kilku pozycji w oknie wyszukiwania.
		FileNameExtensionFilter filter = new FileNameExtensionFilter(".txt", "txt", "text");  //Tutaj określam pliki .txt jako jedyną możliwość wyboru w oknie wyszukiwania.
		fileChooser.setFileFilter(filter);  //Tutaj "nakładam" filtr.
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));  //W tym miejscu wybieram folder domyślny.
        int userSelection = fileChooser.showSaveDialog(this);  //Tutaj następuje wybór pliku
		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			
			FileNameExtensionFilter currentFilter = (FileNameExtensionFilter) fileChooser.getFileFilter();
			String ext = currentFilter.getExtensions()[0];
			
			if (!currentFilter.accept(file)) {  //W tym miejscu pętla "pilnuje" by plik otrzymał format określony przez filtr.
			       
			       String fileName = file.getName();  
			       file = new File(file.getParent(), fileName + "." + ext);
			}

			try {
	    		FileOutputStream fos = new FileOutputStream(file);  //Tutaj następuje zapis do pliku.
	    		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
	    				bw.write(textArea.getText());
	    				bw.newLine();
	    				textArea.append("Zapisano w: " + file.getAbsolutePath());
	    		bw.close();
	    	}catch(Exception e) {
	    		e.printStackTrace();
	    		
	    	}
	    }
			
		}
	}
